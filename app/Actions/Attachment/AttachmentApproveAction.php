<?php

namespace App\Actions\Attachment;

use App\Enums\ApproveStateEnum;
use App\Models\Attachment;

class AttachmentApproveAction
{
    public function execute(Attachment $attachment): Attachment
    {
        $attachment->update([
            'state_enum' => ApproveStateEnum::APPROVED,
        ]);

        return $attachment;
    }
}
