<?php

namespace App\Actions\Auth;

use App\Actions\Business\BusinessCreateAction;
use App\Actions\User\UserCreateAction;
use App\Data\Auth\BusinessRegisterData;
use App\Data\Auth\LoginData;
use App\Data\Business\BusinessCreateData;
use App\Data\User\UserCreateData;
use App\Http\Resources\Auth\UserResource;

class BusinessRegisterAction
{
    public function __construct(
        private UserCreateAction $userCreateAction,
        private BusinessCreateAction $businessCreateAction,
        private GetTokenAction $getTokenAction,
    ) {
    }

    public function execute(BusinessRegisterData $data): array
    {
        $user = $this->userCreateAction->execute(
            new UserCreateData(
                $data->email,
                $data->password,
                $data->first_name . ' ' . $data->last_name,
            ),
        );

        $this->businessCreateAction->execute(
            new BusinessCreateData($user)
        );

        return [
            'token' => $this->getTokenAction->execute(LoginData::from([
                'email' => $data->email,
                'password' => $data->password,
            ])),
            'user'  => UserResource::make($user->load('business')),
        ];
    }
}
