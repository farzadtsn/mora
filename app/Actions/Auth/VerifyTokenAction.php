<?php

namespace App\Actions\Auth;

use App\Models\User;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Facades\Auth;
use Monopone\Auth\AuthService;
use Symfony\Component\HttpFoundation\Response;

class VerifyTokenAction
{
    public function __construct(
        private AuthService $authService
    ) {
    }

    public function execute(): void
    {
        if (empty(request()->bearerToken())) {
            throw new AuthenticationException();
        }

        $response = (new AuthService())->verify();

        if ($response->status() !== Response::HTTP_OK) {
            throw new AuthenticationException();
        }

        Auth::login(User::findOrFail($response['data']['user']['id'] ?? null));
    }
}
