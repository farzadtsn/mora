<?php

namespace App\Actions\Business;

use App\Models\Business;

class BusinessLoadRelationAction
{
    public function execute(Business $business): Business
    {
        return $business->load([
            'categories.ancestors',
            'country',
            'city',
            'state',
            'serviceAreas',
            'gallery',
            'logo',
            'userActions',
            'facilities.facilityGroup',
        ]);
    }
}
