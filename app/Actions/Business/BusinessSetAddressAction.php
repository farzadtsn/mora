<?php

namespace App\Actions\Business;

use App\Data\Business\BusinessAddressData;
use App\Models\City;
use App\Models\User;

class BusinessSetAddressAction
{
    private int $step = 2;

    public function __construct(
        private BusinessValidateStepAction $businessValidateStepAction,
    ) {
    }

    public function execute(BusinessAddressData $businessAddressData, User $user): User
    {
        $this->businessValidateStepAction->execute($user, $this->step);

        $city = City::find($businessAddressData->city_id);

        $user->business
            ->update([
                'address' => $businessAddressData->address,
                'city_id' => $city->id,
                'state_id' => $city->state_id,
                'country_id' => $city->country_id,
                'zip_code' => $businessAddressData->zip_code,
                'step' => $this->step + 1,
            ]);

        return $user;
    }
}
