<?php

namespace App\Actions\Business;

use App\Data\Business\BusinessInfoData;
use App\Models\User;

class BusinessSetInfoAction
{
    public function execute(BusinessInfoData $businessInfoData, User $user): User
    {
        $user->business
            ->update([
                'name' => $businessInfoData->business_name,
                'business_type' => $businessInfoData->business_type,
                'website' => $businessInfoData->website,
                'step' => 2,
            ]);

        $user->business->categories()->sync($businessInfoData->categories);

        return $user;
    }
}
