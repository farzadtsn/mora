<?php

namespace App\Actions\Business;

use App\Actions\Comment\CommentsRatingsStatisticsCalculateAction;
use App\Actions\Comment\CommentsReviewsStatisticsCalculateAction;
use App\Enums\UserActionTypeEnum;
use App\Models\Business;

class BusinessStatisticsAction
{
    public function execute(Business $business): array
    {
        return [
            'user_actions' => [
                'likes_count' => $business
                    ->userActions
                    ->where('type_enum', UserActionTypeEnum::LIKE)
                    ->count(),
                'dislikes_count' => $business
                    ->userActions
                    ->where('type_enum', UserActionTypeEnum::DISLIKE)
                    ->count(),
                'suggests_count' => $business
                    ->userActions
                    ->where('type_enum', UserActionTypeEnum::SUGGEST)
                    ->count(),
            ],
            'reviews' => app(CommentsReviewsStatisticsCalculateAction::class)
                ->execute('businesses', $business->id),
            'ratings' => app(CommentsRatingsStatisticsCalculateAction::class)
                ->execute('businesses', $business->id),
        ];
    }
}
