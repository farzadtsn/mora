<?php

namespace App\Actions\Business;

use App\Models\User;
use Illuminate\Validation\UnauthorizedException;

class BusinessValidateStepAction
{
    public function execute(User $user, int $step): void
    {
        if ($user->business->step && $user->business->step < $step) {
            throw new UnauthorizedException(
                message: trans('messages.previous_steps_are_not_complete'),
            );
        }
    }
}
