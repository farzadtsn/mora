<?php

namespace App\Actions\BusinessProfile;

use App\Data\BusinessProfile\BusinessGalleryData;
use App\Enums\AttachmentTypeEnum;
use App\Models\User;

class BusinessAddToGalleryAction
{
    public function execute(BusinessGalleryData $businessGalleryData, User $user): User
    {
        $attachment = $user->business
            ->gallery()
            ->create([
                'type_enum' => AttachmentTypeEnum::BUSINESS_GALLERY,
                'title' => $businessGalleryData->title,
                'position' => $businessGalleryData->position ?: 1,
            ]);

        $attachment->addMedia($businessGalleryData->photo)
            ->toMediaCollection(AttachmentTypeEnum::BUSINESS_GALLERY);

        return $user;
    }
}
