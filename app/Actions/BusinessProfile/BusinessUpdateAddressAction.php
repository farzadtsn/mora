<?php

namespace App\Actions\BusinessProfile;

use App\Data\BusinessProfile\BusinessAddressData;
use App\Models\City;
use App\Models\User;

class BusinessUpdateAddressAction
{
    public function execute(BusinessAddressData $businessAddressData, User $user): User
    {
        $city = City::find($businessAddressData->city_id);

        $user->business
            ->update([
                'address' => $businessAddressData->address,
                'city_id' => $city->id,
                'state_id' => $city->state_id,
                'country_id' => $city->country_id,
                'zip_code' => $businessAddressData->zip_code,
                'latitude' => $businessAddressData->latitude,
                'longitude' => $businessAddressData->longitude,
            ]);

        $user->business->serviceAreas()->sync($businessAddressData->service_areas);

        return $user;
    }
}
