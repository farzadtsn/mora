<?php

namespace App\Actions\BusinessProfile;

use App\Data\BusinessProfile\BusinessBioData;
use App\Models\User;

class BusinessUpdateBioAction
{
    public function execute(BusinessBioData $businessBioData, User $user): User
    {
        $user->business
            ->update([
                'description' => $businessBioData->description,
                'owner_name' => $businessBioData->owner_name,
                'established_year' => $businessBioData->established_year,
            ]);

        return $user;
    }
}
