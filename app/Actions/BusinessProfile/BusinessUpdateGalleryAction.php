<?php

namespace App\Actions\BusinessProfile;

use App\Data\BusinessProfile\BusinessUpdateGalleryData;
use App\Models\Attachment;
use App\Models\User;
use Illuminate\Validation\UnauthorizedException;

class BusinessUpdateGalleryAction
{
    public function execute(
        BusinessUpdateGalleryData $businessGalleryData,
        Attachment $attachment,
        User $user
    ): User {
        if (!$attachment->owner?->is($user->business)) {
            throw new UnauthorizedException();
        }
        $attachment->update([
                'title' => $businessGalleryData->title,
                'position' => $businessGalleryData->position ?: 1,
            ]);

        return $user;
    }
}
