<?php

namespace App\Actions\BusinessProfile;

use App\Data\BusinessProfile\BusinessOpeningHoursData;
use App\Models\User;

class BusinessUpdateOpeningHoursAction
{
    public function execute(
        BusinessOpeningHoursData $businessOpeningHoursData,
        User $user
    ): User {
        $user->business
            ->update([
                'opening_hours' => $businessOpeningHoursData->opening_hours,
            ]);

        return $user;
    }
}
