<?php

namespace App\Actions\BusinessProfile;

use App\Data\BusinessProfile\BusinessReopenDateData;
use App\Models\User;

class BusinessUpdateReopenDateAction
{
    public function execute(
        BusinessReopenDateData $businessReopenDateData,
        User $user
    ): User {
        $user->business
            ->update([
                'reopen_date' => $businessReopenDateData->reopen_date,
                'is_closed' => true,
                'closed_reason' => $businessReopenDateData->reason,
            ]);

        return $user;
    }
}
