<?php

namespace App\Actions\Currency;

use App\Exceptions\ReadOnlyException;
use App\Models\Currency;

class CurrencyDeleteAction
{
    public function execute(Currency $currency): void
    {
        if (!$currency->canBeDeleted()) {
            throw new ReadOnlyException();
        }

        $currency->delete();
    }
}
