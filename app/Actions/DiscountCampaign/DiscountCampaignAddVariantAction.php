<?php

namespace App\Actions\DiscountCampaign;

use App\Data\DiscountCampaign\DiscountCampaignVariantData;
use App\Models\Business;
use App\Models\DiscountCampaign;

class DiscountCampaignAddVariantAction
{
    private int $step = 5;

    public function __construct(
        private DiscountCampaignCreateAction $discountCampaignCreateAction,
        private DiscountCampaignValidateStepAction $discountCampaignValidateStepAction
    ) {
    }

    public function execute(
        DiscountCampaignVariantData $data,
        Business $business,
    ): DiscountCampaign {
        $discountCampaign = $this->discountCampaignCreateAction->execute($business);

        $this->discountCampaignValidateStepAction->execute(
            $discountCampaign,
            $this->step,
        );

        $discountCampaign->variants()
            ->create([
                'name' => $data->name,
                'price' => $data->price,
                'discount_percent' => $data->discount_percent,
                'quantity' => $data->quantity,
                'remained_quantity' => $data->quantity,
            ]);

        $discountCampaign->update([
            'step' => $this->step + 1,
        ]);

        return $discountCampaign;
    }
}
