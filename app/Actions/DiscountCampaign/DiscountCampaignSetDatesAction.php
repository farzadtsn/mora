<?php

namespace App\Actions\DiscountCampaign;

use App\Data\DiscountCampaign\DiscountCampaignDateData;
use App\Data\DiscountCampaign\DiscountCampaignDescriptionData;
use App\Enums\DiscountCampaignStateEnum;
use App\Models\Business;
use App\Models\DiscountCampaign;

class DiscountCampaignSetDatesAction
{
    private int $step = 8;

    public function __construct(
        private DiscountCampaignCreateAction $discountCampaignCreateAction,
        private DiscountCampaignValidateStepAction $discountCampaignValidateStepAction
    ) {
    }

    public function execute(
        DiscountCampaignDateData $data,
        Business $business,
    ): DiscountCampaign {
        $discountCampaign = $this->discountCampaignCreateAction->execute($business);

        $this->discountCampaignValidateStepAction->execute(
            $discountCampaign,
            $this->step,
        );

        $discountCampaign->update([
            'step' => null,
            'state_enum' => DiscountCampaignStateEnum::PUBLISHED,
            'start_date' => $data->start_date,
            'end_date' => $data->end_date,
            'deadline' => $data->deadline,
        ]);

        return $discountCampaign;
    }
}
