<?php

namespace App\Actions\Search\Business;

use App\Data\Business\BusinessFilterData;
use Elastic\ScoutDriverPlus\Builders\BoolQueryBuilder;
use Elastic\ScoutDriverPlus\Support\Query;

class ApplyFilersAction
{
    private BusinessFilterData $filters;

    public function execute(BusinessFilterData $filters): ?BoolQueryBuilder
    {
        $this->filters = $filters;

        if (count(array_filter($filters->all())) === 0) {
            return null;
        }

        $query = Query::bool();

        $query = $this->searchTerm($query);
        $query = $this->filterCountry($query);
        $query = $this->filterCity($query);
        $query = $this->filterCategories($query);

        return $query;
    }

    private function searchTerm(BoolQueryBuilder $query): BoolQueryBuilder
    {
        if ($this->filters->q) {
            $query->must(
                Query::wildcard()
                    ->field('name')
                    ->value(strtolower($this->filters->q) . '*')
            );
        }

        return $query;
    }

    private function filterCountry(BoolQueryBuilder $query): BoolQueryBuilder
    {
        if ($this->filters->countries) {
            $query->must(
                Query::terms()
                    ->field('country.id')
                    ->values($this->filters->countries)
            );
        }

        return $query;
    }

    private function filterCity(BoolQueryBuilder $query): BoolQueryBuilder
    {
        if ($this->filters->cities) {
            $query->must(
                Query::terms()
                    ->field('city.id')
                    ->values($this->filters->cities)
            );
        }

        return $query;
    }

    private function filterCategories(BoolQueryBuilder $query): BoolQueryBuilder
    {
        if ($this->filters->categories) {
            $query->must(
                Query::nested()
                    ->path('categories')
                    ->query(
                        Query::terms()
                            ->field('categories.parent_ids')
                            ->values($this->filters->categories)
                    )
            );
        }

        return $query;
    }
}
