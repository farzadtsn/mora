<?php

namespace App\Actions\UserAction;

use App\Models\Business;
use App\Models\Comment;
use App\Models\User;

class UserActionRemoveAction
{
    public function execute(
        Business|Comment $actionable,
        User $user,
        string $action
    ):void {
        $actionable->userActions()->where([
            'type_enum' => $action,
            'user_id' => $user->id,
        ])->firstOrFail()->delete();
    }
}
