<?php

namespace App\Console\Commands;

use App\Models\Business;
use App\Models\DiscountCampaign;
use Illuminate\Console\Command;

class ElasticsearchIndexCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mono:elastic';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->call('elastic:migrate:refresh');
        $this->call('scout:flush', ['model' => Business::class]);
        $this->call('scout:flush', ['model' => DiscountCampaign::class]);
        Business::whereNull('step')->searchable();
        DiscountCampaign::query()->searchable();

        return 0;
    }
}
