<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class Installer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install platform';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $connection = config('database.default');

        $schema = config('database.connections.' . $connection . '.schema');
        $username = config('database.connections.' . $connection . '.username');

        DB::statement('CREATE SCHEMA IF NOT EXISTS "' . $schema . '" AUTHORIZATION "' . $username . '";');

        Artisan::call('migrate');

        $this->info('Install platform successfully!');
    }
}
