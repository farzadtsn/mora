<?php

namespace App\Data\Attachment;

use App\Data\Filter\FilterData;
use App\Enums\ApproveStateEnum;
use App\Enums\AttachmentTypeEnum;
use App\Enums\HtmlInputTypeEnum;
use Illuminate\Validation\Rule;
use Spatie\LaravelData\Data;

class AttachmentFilterData extends Data
{
    public function __construct(
        public readonly ?array $type_enums,
        public readonly ?array $state_enums,
    ) {
    }

    public static function rules(): array
    {
        return [
            'type_enums' => ['array'],
            'type_enums.*' => [Rule::in(AttachmentTypeEnum::getValues())],
            'state_enums' => ['array'],
            'state_enums.*' => [Rule::in(ApproveStateEnum::getValues())],
        ];
    }

    public static function getSelectableFilters(): array
    {
        return [
            FilterData::from([
                'name' => 'type_enums',
                'html_input_type' => HtmlInputTypeEnum::MULTISELECT,
                'cast_type' => 'string',
                'values' => AttachmentTypeEnum::labels(),
            ]),
            FilterData::from([
                'name' => 'state_enums',
                'html_input_type' => HtmlInputTypeEnum::MULTISELECT,
                'cast_type' => 'string',
                'values' => ApproveStateEnum::labels(),
            ]),
        ];
    }
}
