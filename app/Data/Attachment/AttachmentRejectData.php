<?php

namespace App\Data\Attachment;

use Spatie\LaravelData\Data;

class AttachmentRejectData extends Data
{
    public function __construct(
        public readonly string $reject_reason,
    ) {
    }

    public static function rules(): array
    {
        return [
            'reject_reason' => ['required', 'min:2', 'max:200'],
        ];
    }
}
