<?php

namespace App\Data\Auth;

use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Password;
use Spatie\LaravelData\Data;

class RegisterData extends Data
{
    public function __construct(
        public readonly string $name,
        public readonly string $email,
        public readonly string $password,
    ) {
    }

    public static function rules(): array
    {
        return [
            'name' => ['required', 'string', 'min:4', 'max:50', "regex:/^([a-zA-Z' ]+)$/"],
            'email' => ['required', 'email', Rule::unique('users')],
            'password' => ['required', 'string', 'min:8', Password::min(8)->mixedCase()->symbols()->numbers()],
        ];
    }
}
