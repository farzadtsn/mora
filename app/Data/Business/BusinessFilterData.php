<?php

namespace App\Data\Business;

use Spatie\LaravelData\Data;

class BusinessFilterData extends Data
{
    public function __construct(
        public readonly ?string $q,
        public readonly ?array $countries,
        public readonly ?string $country_code,
        public readonly ?array $cities,
        public readonly ?array $categories,
        public readonly ?LocationData $location,
        public readonly ?int $rating,
        public readonly ?float $min_price,
        public readonly ?float $max_price,
        public readonly ?bool $in_stock,
    ) {
    }

    public static function rules(): array
    {
        return [
            'q' => ['string', 'min:2', 'max:200'],
            'countries' => ['array'],
            'countries.*' => ['integer'],
            'cities' => ['array'],
            'cities.*' => ['integer'],
            'categories' => ['array'],
            'categories.*' => ['integer'],
            'rating' => ['integer'],
            'min_price' => ['numeric'],
            'max_price' => ['numeric', 'gte:min_price'],
            'in_stock' => ['boolean'],
            'country_code' => ['string'],
        ];
    }
}
