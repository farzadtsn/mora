<?php

namespace App\Data\Business;

use App\Enums\BusinessTypeEnum;
use Illuminate\Validation\Rule;
use Spatie\LaravelData\Data;

class BusinessInfoData extends Data
{
    public function __construct(
        public readonly string $business_name,
        public readonly array $categories,
        public readonly string $business_type,
        public readonly ?string $website,
    ) {
    }

    public static function rules(): array
    {
        return [
            'business_name' => ['required', 'min:2'],
            'business_type' => ['required', Rule::in(BusinessTypeEnum::getValues())],
            'website' => ['url'],
            'categories' => ['required', 'array', 'min:1'],
            'categories.*' => ['required', Rule::exists('categories', 'id')],
        ];
    }
}
