<?php

namespace App\Data\Business;

use Spatie\LaravelData\Data;

class DiscountCampaignVariantSearchData extends Data
{
    public function __construct(
        public readonly string $name,
        public readonly string $price,
        public readonly string $discount_percent,
        public readonly string $discounted_price,
        public readonly int $quantity,
        // public readonly int $remained_quantity,
    ) {
    }

    public function formattedResponse(): array
    {
        return [
            'name' => $this->name,
            'price' => $this->price,
            'discounted_price' => $this->discounted_price,
            'discount_percent' => $this->discount_percent,
            'quantity' => $this->quantity,
            'remained_quantity' => $this->remained_quantity,
        ];
    }
}
