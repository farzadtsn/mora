<?php

namespace App\Data\BusinessProfile;

use Illuminate\Http\UploadedFile;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\File;
use Spatie\LaravelData\Data;

class BusinessBasicInfoData extends Data
{
    public function __construct(
        public readonly string $business_name,
        public readonly string $phone,
        public readonly ?string $website,
        public readonly UploadedFile $logo,
    ) {
    }

    public static function rules(): array
    {
        $countryCode = auth()->user()->business->country->iso2;

        return [
            'business_name' => ['required', 'min:2'],
            'phone' => [
                'nullable',
                Rule::phone()->country([$countryCode])->mobile(),
                'regex:/^[0-9]+$/',
            ],
            'website' => ['url'],
            'logo' => [
                'required',
                File::image(),
                    // ->min(40)
                    // ->max(400)
                    // ->dimensions(Rule::dimensions()->maxWidth(1000)->maxHeight(1000)),
            ],
        ];
    }
}
