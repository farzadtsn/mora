<?php

namespace App\Data\BusinessProfile;

use Illuminate\Validation\Rule;
use Spatie\LaravelData\Data;

class BusinessFacilitiesData extends Data
{
    public function __construct(
        public readonly array $facilities,
    ) {
    }

    public static function rules(): array
    {
        return [
            'facilities' => ['required', 'array', 'min:1'],
            'facilities.*' => ['required', Rule::exists('facilities', 'id'), 'distinct'],
        ];
    }
}
