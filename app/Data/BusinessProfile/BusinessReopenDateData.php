<?php

namespace App\Data\BusinessProfile;

use Spatie\LaravelData\Data;

class BusinessReopenDateData extends Data
{
    public function __construct(
        public readonly ?string $reopen_date,
        public readonly string $reason,
    ) {
    }

    public static function rules(): array
    {
        return [
            'reopen_date' => ['nullable', 'date_format:Y-m-d', 'after:today'],
            'reason' => ['required', 'min:4', 'max:1000'],
        ];
    }
}
