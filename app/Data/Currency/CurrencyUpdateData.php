<?php

namespace App\Data\Currency;

use Illuminate\Validation\Rule;
use Spatie\LaravelData\Data;

class CurrencyUpdateData extends Data
{
    // public readonly ?int $currency_id;

    public function __construct(
        public readonly string $name,
        public readonly string $symbol,
        public readonly string $iso_code,
        public readonly float $exchange_rate,
        // $currency_id = null,
    ) {
        // $this->currency_id = $currency_id ?: request('currency');
    }

    public static function rules(): array
    {
        return [
            'name' => ['required', 'string', 'min:2'],
            'symbol' => ['required', 'string', 'min:2'],
            'iso_code' => [
                'required',
                'string',
                'min:2',
                'max:14',
                Rule::unique('currencies', 'iso_code')
                    ->ignore(request('currency')->id)
                    ->where('deleted_at', null),
            ],
            'exchange_rate' => ['required', 'numeric', 'between:0.00001,99999'],
        ];
    }
}
