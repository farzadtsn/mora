<?php

namespace App\Data\DiscountCampaign;

use Spatie\LaravelData\Data;

class DiscountCampaignDateData extends Data
{
    public function __construct(
        public readonly string $start_date,
        public readonly string $end_date,
        public readonly int $deadline,
    ) {
    }

    public static function rules(): array
    {
        return [
            'start_date' => ['required', 'date', 'date_format:Y-m-d', 'after:now'],
            'end_date' => ['required', 'date', 'date_format:Y-m-d', 'after:start_date'],
            'deadline' => ['required', 'integer', 'min:1', 'max:9999'],
        ];
    }
}
