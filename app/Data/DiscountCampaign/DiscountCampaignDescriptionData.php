<?php

namespace App\Data\DiscountCampaign;

use Spatie\LaravelData\Data;

class DiscountCampaignDescriptionData extends Data
{
    public function __construct(
        public readonly string $description,
    ) {
    }

    public static function rules(): array
    {
        return [
            'description' => ['required', 'string', 'min:3', 'max:20000'],
        ];
    }
}
