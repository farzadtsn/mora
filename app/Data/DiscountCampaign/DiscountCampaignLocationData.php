<?php

namespace App\Data\DiscountCampaign;

use App\Enums\DiscountCampaignLocationEnum;
use Illuminate\Validation\Rule;
use Spatie\LaravelData\Data;

class DiscountCampaignLocationData extends Data
{
    public function __construct(
        public readonly string $location,
    ) {
    }

    public static function rules(): array
    {
        return [
            'location' => ['required', Rule::in(DiscountCampaignLocationEnum::getValues())],
        ];
    }
}
