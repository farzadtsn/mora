<?php

namespace App\Data\DiscountCampaign;

use Illuminate\Http\UploadedFile;
use Illuminate\Validation\Rules\File;
use Spatie\LaravelData\Data;

class DiscountCampaignPhotoData extends Data
{
    public function __construct(
        public readonly UploadedFile $photo,
    ) {
    }

    public static function rules(): array
    {
        return [
            'photo' => [
                'required',
                File::image(),
                    // ->min(40)
                    // ->max(400)
                    // ->dimensions(Rule::dimensions()->maxWidth(1000)->maxHeight(1000)),
            ],
        ];
    }
}
