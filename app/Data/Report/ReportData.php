<?php

namespace App\Data\Report;

use App\Enums\PhotoReportEnum;
use Illuminate\Validation\Rule;
use Spatie\LaravelData\Data;

class ReportData extends Data
{
    public function __construct(
        public readonly string $subject,
        public readonly ?string $description,
    ) {
    }

    public static function rules(): array
    {
        return [
            'subject' => ['required', Rule::in(PhotoReportEnum::getValues())],
            'description' => ['string', 'max:300'],
        ];
    }
}
