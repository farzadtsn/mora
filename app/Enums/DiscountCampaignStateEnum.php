<?php

namespace App\Enums;

class DiscountCampaignStateEnum extends BaseEnum
{
    const PENDING = 'pending';
    const REVIEW = 'review';
    const PUBLISHED = 'published';
    const RETURNED = 'returned';
    const REJECTED = 'rejected';
}
