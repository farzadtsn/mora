<?php

namespace App\Http\Controllers\BaseInfo;

use App\Http\Controllers\Controller;
use App\Http\Resources\BaseInfo\Category\CategoryResource;
use App\Http\Resources\BaseInfo\Category\CategoryWithRootResource;
use App\Models\Category;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Response;

class CategoryController extends Controller
{
    public function treeView()
    {
        return Response::success(
            message: '',
            data:  CategoryResource::collection(
                Category::get()->toTree()
            ),
        );
    }

    public function root(): JsonResponse
    {
        return Response::success(
            message: '',
            data:  CategoryResource::collection(
                Category::whereIsRoot()->get()
            ),
        );
    }

    public function leaf(): JsonResponse
    {
        $categories = Category::query()
            ->whereIsLeaf()
            ->leafWithRoot()
            ->get();

        return Response::success(
            message: '',
            data:  CategoryWithRootResource::collection($categories),
        );
    }
}
