<?php

namespace App\Http\Controllers\BaseInfo;

use App\Http\Controllers\Controller;
use App\Http\Resources\Client\PaymentGateway\PaymentGatewayResource;
use App\Models\PaymentGateway;
use Illuminate\Support\Facades\Response;

class PaymentGatewayController extends Controller
{
    public function index()
    {
        return Response::success(
            message: '',
            data: PaymentGatewayResource::collection(
                PaymentGateway::with('currencies')->get()
            ),
        );
    }
}
