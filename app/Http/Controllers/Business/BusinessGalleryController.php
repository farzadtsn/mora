<?php

namespace App\Http\Controllers\Business;

use App\Actions\Business\BusinessLoadRelationAction;
use App\Actions\BusinessProfile\BusinessAddToGalleryAction;
use App\Actions\BusinessProfile\BusinessRemoveFromGalleryAction;
use App\Actions\BusinessProfile\BusinessUpdateGalleryAction;
use App\Data\BusinessProfile\BusinessGalleryData;
use App\Data\BusinessProfile\BusinessUpdateGalleryData;
use App\Http\Controllers\Controller;
use App\Http\Resources\Business\Business\UserResource;
use App\Models\Attachment;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Response;

class BusinessGalleryController extends Controller
{
    public function addToGallery(
        BusinessGalleryData $data,
        BusinessAddToGalleryAction $businessAddToGalleryAction,
        BusinessLoadRelationAction $businessLoadRelationAction,
    ) :JsonResponse {
        $user = $businessAddToGalleryAction->execute($data, auth()->user());
        $businessLoadRelationAction->execute($user->business);

        return Response::success(
            message: '',
            data:  UserResource::make($user),
        );
    }

    public function updatePhotoData(
        BusinessUpdateGalleryData $data,
        BusinessUpdateGalleryAction $businessAddToGalleryAction,
        BusinessLoadRelationAction $businessLoadRelationAction,
        Attachment $attachment,
    ) :JsonResponse {
        $user = $businessAddToGalleryAction->execute($data, $attachment, auth()->user());
        $businessLoadRelationAction->execute($user->business);

        return Response::success(
            message: '',
            data:  UserResource::make($user),
        );
    }

    public function removeFromGallery(
        BusinessRemoveFromGalleryAction $businessRemoveFromGalleryAction,
        BusinessLoadRelationAction $businessLoadRelationAction,
        Attachment $attachment
    ) :JsonResponse {
        $user = $businessRemoveFromGalleryAction->execute($attachment, auth()->user());
        $businessLoadRelationAction->execute($user->business);

        return Response::success(
            message: trans('messages.deleted_successfully'),
            data:  UserResource::make($user),
        );
    }
}
