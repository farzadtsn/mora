<?php

namespace App\Http\Controllers\Business;

use App\Actions\Business\BusinessLoadRelationAction;
use App\Actions\BusinessProfile\BusinessUpdateAddressAction;
use App\Actions\BusinessProfile\BusinessUpdateBasicInfoAction;
use App\Actions\BusinessProfile\BusinessUpdateBioAction;
use App\Actions\BusinessProfile\BusinessUpdateCategoriesAction;
use App\Actions\BusinessProfile\BusinessUpdateFacilitiesAction;
use App\Actions\BusinessProfile\BusinessUpdateOpeningHoursAction;
use App\Actions\BusinessProfile\BusinessUpdateReopenDateAction;
use App\Data\BusinessProfile\BusinessAddressData;
use App\Data\BusinessProfile\BusinessBasicInfoData;
use App\Data\BusinessProfile\BusinessBioData;
use App\Data\BusinessProfile\BusinessCategoriesData;
use App\Data\BusinessProfile\BusinessFacilitiesData;
use App\Data\BusinessProfile\BusinessOpeningHoursData;
use App\Data\BusinessProfile\BusinessReopenDateData;
use App\Http\Controllers\Controller;
use App\Http\Resources\Business\Business\UserResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Response;

class BusinessProfileController extends Controller
{
    public function showProfile(BusinessLoadRelationAction $businessLoadRelationAction)
    {
        $businessLoadRelationAction->execute(auth()->user()->business);

        return Response::success(
            message: '',
            data:  UserResource::make(auth()->user()),
        );
    }

    public function updateBasicInfo(
        BusinessBasicInfoData $data,
        BusinessUpdateBasicInfoAction $businessUpdateInfoAction,
        BusinessLoadRelationAction $businessLoadRelationAction
    ) :JsonResponse {
        $user = $businessUpdateInfoAction->execute($data, auth()->user());
        $businessLoadRelationAction->execute($user->business);

        return Response::success(
            message: '',
            data:  UserResource::make($user),
        );
    }

    public function updateAddress(
        BusinessAddressData $data,
        BusinessUpdateAddressAction $businessUpdateAddressAction,
        BusinessLoadRelationAction $businessLoadRelationAction
    ) :JsonResponse {
        $user = $businessUpdateAddressAction->execute($data, auth()->user());
        $businessLoadRelationAction->execute($user->business);

        return Response::success(
            message: '',
            data:  UserResource::make($user),
        );
    }

    public function updateOpeningHours(
        BusinessOpeningHoursData $data,
        BusinessUpdateOpeningHoursAction $businessUpdateOpeningHoursAction,
        BusinessLoadRelationAction $businessLoadRelationAction
    ) :JsonResponse {
        $user = $businessUpdateOpeningHoursAction->execute($data, auth()->user());
        $businessLoadRelationAction->execute($user->business);

        return Response::success(
            message: '',
            data:  UserResource::make($user),
        );
    }

    public function closeBusiness(
        BusinessReopenDateData $data,
        BusinessUpdateReopenDateAction $businessUpdateReopenDateAction,
        BusinessLoadRelationAction $businessLoadRelationAction
    ) :JsonResponse {
        $user = $businessUpdateReopenDateAction->execute($data, auth()->user());
        $businessLoadRelationAction->execute($user->business);

        return Response::success(
            message: '',
            data:  UserResource::make($user),
        );
    }

    public function updateFacilities(
        BusinessFacilitiesData $data,
        BusinessUpdateFacilitiesAction $businessUpdateFacilitiesAction,
        BusinessLoadRelationAction $businessLoadRelationAction
    ) :JsonResponse {
        $user = $businessUpdateFacilitiesAction->execute($data, auth()->user());
        $businessLoadRelationAction->execute($user->business);

        return Response::success(
            message: '',
            data:  UserResource::make($user),
        );
    }

    public function updateCategories(
        BusinessCategoriesData $data,
        BusinessUpdateCategoriesAction $businessUpdateCategoriesAction,
        BusinessLoadRelationAction $businessLoadRelationAction
    ) :JsonResponse {
        $user = $businessUpdateCategoriesAction->execute($data, auth()->user());
        $businessLoadRelationAction->execute($user->business);

        return Response::success(
            message: '',
            data:  UserResource::make($user),
        );
    }

    public function updateBio(
        BusinessBioData $data,
        BusinessUpdateBioAction $businessUpdateBioAction,
        BusinessLoadRelationAction $businessLoadRelationAction
    ) :JsonResponse {
        $user = $businessUpdateBioAction->execute($data, auth()->user());
        $businessLoadRelationAction->execute($user->business);

        return Response::success(
            message: '',
            data:  UserResource::make($user),
        );
    }
}
