<?php

namespace App\Http\Controllers\Client;

use App\Actions\UserAction\UserActionAddAction;
use App\Actions\UserAction\UserActionRemoveAction;
use App\Http\Controllers\Controller;
use App\Models\Business;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Response;

class BusinessUserActionController extends Controller
{
    public function addAction(
        UserActionAddAction $userActionAddAction,
        Business $business,
        string $action,
    ) :JsonResponse {
        $userActionAddAction->execute($business, auth()->user(), $action);

        return Response::success(
            message: trans('messages.action_added'),
            data: [],
        );
    }

    public function removeAction(
        UserActionRemoveAction $userActionRemoveAction,
        Business $business,
        string $action,
    ) :JsonResponse {
        $userActionRemoveAction->execute($business, auth()->user(), $action);

        return Response::success(
            message: trans('messages.action_removed'),
            data: [],
        );
    }
}
