<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Monopone\Auth\AuthService;

class CanBeGuestMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if (empty(request()->bearerToken())) {
            return $next($request);
        }

        $response = (new AuthService())->verify();

        if (isset($response['data']['user']['id'])) {
            Auth::login(User::findOrFail($response['data']['user']['id'] ?? null));
        }

        return $next($request);
    }
}
