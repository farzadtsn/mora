<?php

namespace App\Http\Resources\Admin\Attachment;

use Illuminate\Http\Resources\Json\JsonResource;

class AttachmentResource extends JsonResource
{
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
