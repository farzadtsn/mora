<?php

namespace App\Http\Resources\Admin\Currency;

use Illuminate\Http\Resources\Json\JsonResource;

class CurrencyResource extends JsonResource
{
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
