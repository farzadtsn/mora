<?php

namespace App\Http\Resources\BaseInfo\Category;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'children' => self::collection($this->whenLoaded('children')),
            'parents' => self::collection($this->whenLoaded('ancestors')),
        ];
    }
}
