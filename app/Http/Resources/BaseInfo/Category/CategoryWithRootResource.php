<?php

namespace App\Http\Resources\BaseInfo\Category;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoryWithRootResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'children' => self::collection($this->whenLoaded('children')),
            'root' => CategoryResource::make($this->ancestors->first()),
        ];
    }
}
