<?php

namespace App\Http\Resources\Client\Attachment;

use App\Http\Resources\Client\Media\MediaResource;
use Illuminate\Http\Resources\Json\JsonResource;

class AttachmentResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'position' => $this->position,
            'state_enum' => $this->state_enum,
            'media' => MediaResource::make($this->getFirstMedia($this->type_enum)),
        ];
    }
}
