<?php

namespace App\Http\Resources\Client\Comment;

use App\Http\Resources\Auth\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class CommentResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'body' => $this->body,
            'statistics' => [
                'rating' => $this->rating,
                'likes_count' => $this->likes_count,
                'dislikes_count' => $this->dislikes_count,
            ],
            'user' => UserResource::make($this->whenLoaded('user')),
        ];
    }
}
