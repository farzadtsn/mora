<?php

namespace App\Http\Resources\Client\Transaction;

use App\Http\Resources\Client\PaymentGateway\PaymentGatewayResource;
use Illuminate\Http\Resources\Json\JsonResource;

class TransactionResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'amount' => $this->amount,
            'state' => $this->stateWithColor(),
            'invoice_number' => $this->invoice_number,
            'created_at' => $this->created_at,
            'payment_gateway' => PaymentGatewayResource::make($this->whenLoaded('paymentGateway')),
        ];
    }
}
