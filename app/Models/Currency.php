<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Currency extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];

    public function UserBalances()
    {
        return $this->hasMany(UserBalance::class, 'currency_id');
    }

    public function latestBalance()
    {
        return $this->hasOne(UserBalance::class)->ofMany([
            'id' => 'max',
        ], function ($query) {
            $query->where('user_id', '=', auth()->id());
        });
    }

    public function paymentGateways()
    {
        return $this->belongsToMany(PaymentGateway::class);
    }

    public function canBeDeleted()
    {
        if ($this->paymentGateways()->exists()) {
            return false;
        }

        return true;
    }
}
