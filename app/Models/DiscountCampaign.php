<?php

namespace App\Models;

use App\Enums\AttachmentTypeEnum;
use App\Enums\DiscountCampaignStateEnum;
use Elastic\ScoutDriverPlus\Searchable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DiscountCampaign extends Model
{
    use HasFactory;
    use Searchable;

    protected $guarded = [];

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function variants()
    {
        return $this->hasMany(DiscountCampaignVariant::class);
    }

    public function business()
    {
        return $this->belongsTo(Business::class);
    }

    public function photos()
    {
        return $this->morphMany(Attachment::class, 'model')
            ->where('type_enum', AttachmentTypeEnum::DISCOUNT_CAMPAIGN_PHOTO)
            ->orderBy('position', 'asc');
    }

    //Elastic
    public function searchableAs()
    {
        return 'discount_campaign_index';
    }

    public function shouldBeSearchable()
    {
        return true;

        return $this->state_enum === DiscountCampaignStateEnum::PUBLISHED;
    }

    public function searchableWith()
    {
        return [
            'business.country',
            'business.city',
            'business.state',
            'business.categories',
            'business.logo',
            'variants',
        ];
    }

    public function toSearchableArray()
    {
        $categories = $this->categories->map(function (Category $category) {
            return [
                'id' => $category->id,
                'name' => $category->getTranslations('name'),
                'parent_ids' => $category
                    ->ancestors
                    ->pluck('id')
                    ->prepend($category->id)
                    ->toArray(),
            ];
        })
        ->toArray();

        $discountData = [
            'name' => $this->name,
            'description' => $this->description,
            'location_enum' => $this->location_enum,
            'state_enum' => $this->state_enum,
            'is_appointment_required' => $this->is_appointment_required,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'deadline' => $this->deadline,
            'created_at' => $this->created_at,
            'categories' => $categories,
            'business' => $this->business->toSearchableArray(),
            'variants' => $this->variants->map(function ($variant) {
                return $variant->toSearchableArray();
            }),
        ];

        return $discountData;
    }
}
