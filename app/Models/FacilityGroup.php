<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class FacilityGroup extends Model
{
    use HasFactory, HasTranslations;

    public $translatable = [
        'name',
    ];

    protected $guarded = [];

    public function facilities()
    {
        return $this->hasMany(Facility::class)->orderBy('position', 'asc');
    }
}
