<?php

namespace App\Models\QueryBuilders;

use App\Data\Attachment\AttachmentFilterData;
use Illuminate\Database\Eloquent\Builder;

class AttachmentQueryBuilder extends Builder
{
    public function filter(AttachmentFilterData $filters): self
    {
        return $this
            ->when($filters->type_enums, function ($query, $typeEnums) {
                $query->whereIn('type_enum', $typeEnums);
            })
            ->when($filters->state_enums, function ($query, $stateEnums) {
                $query->whereIn('state_enum', $stateEnums);
            });
    }

    public function sort(?array $sort): self
    {
        if (is_null($sort)) {
            return $this;
        }

        $field = $sort['field'];
        $direction = $sort['direction'];

        return $this
            ->when($field === 'created_at', function ($query) use ($direction) {
                return $query->orderBy('id', $direction);
            })
            ->when($field === 'reports_count', function ($query) use ($direction) {
                return $query->orderBy('reports_count', $direction);
            });
    }
}
