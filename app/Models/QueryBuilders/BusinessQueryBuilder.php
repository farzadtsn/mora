<?php

namespace App\Models\QueryBuilders;

use App\Enums\CommentSortEnum;
use Kalnoy\Nestedset\QueryBuilder;

class BusinessQueryBuilder extends QueryBuilder
{
    public function withStatistics()
    {
        return $this
                ->withCount('likes')
                ->withCount('dislikes')
                ->withCount('suggests');
    }

    public function sort(?array $sort): self
    {
        if (is_null($sort)) {
            return $this;
        }

        $direction = $sort['direction'];
        $field = $sort['field'];

        return $this
            ->when($field === CommentSortEnum::DATE, function ($query) use ($direction) {
                return $query->orderBy('id', $direction);
            })
            ->when($field === CommentSortEnum::RATING, function ($query) use ($direction) {
                return $query->orderBy('rating', $direction);
            })
            ->when($field === CommentSortEnum::LIKES_COUNT, function ($query) use ($direction) {
                return $query->orderBy('likes_count', $direction);
            })
            ->when($field === CommentSortEnum::DISLIKES_COUNT, function ($query) use ($direction) {
                return $query->orderBy('dislikes_count', $direction);
            });
    }
}
