<?php

namespace App\Models;

use App\Enums\TransactionStateEnum;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserTransaction extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }

    public function paymentGateway()
    {
        return $this->belongsTo(PaymentGateway::class);
    }

    public function stateWithColor()
    {
        return [
            'name' => $this->state,
            'color' => TransactionStateEnum::color($this->state),
        ];
    }

    protected function amount(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => round($value, 2),
        );
    }
}
