<?php

namespace App\Providers;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;
use Symfony\Component\HttpFoundation\Response as HttpFoundationResponse;

class ResponseServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro(
            'general',
            function ($message, $data, $code, $success): JsonResponse {
                $code = $code < 1000 && $code >= 200 ? $code : 400;

                $result = [
                    'success' => $success,
                    'code'    => $code,
                    'message' => $message,
                    'data'    => $data,
                ];

                if (isset(request()->user()->role)) {
                    $result['role'] = request()->user()->role;
                }

                return response()->json($result, $code);
            }
        );

        Response::macro(
            'success',
            function (
                $message,
                $data = [],
                $code = HttpFoundationResponse::HTTP_OK,
                $success = true
            ): JsonResponse {
                return response()->general($message, $data, $code, $success);
            }
        );

        Response::macro(
            'error',
            function (
                $message,
                $data = [],
                $code = HttpFoundationResponse::HTTP_BAD_REQUEST
            ): JsonResponse {
                return response()->general($message, $data, $code, false);
            }
        );
    }
}
