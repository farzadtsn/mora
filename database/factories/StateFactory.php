<?php

namespace Database\Factories;

use App\Models\Country;
use App\Models\State;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\State>
 */
class StateFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => $this->faker->city(),
            'country_id' => Country::factory(),
            'country_code' => 'ET',
            'fips_code' => '54',
            'iso2' => 'SN',
            'latitude' => '6.51569110',
            'longitude' => '36.95410700',
            'flag' => 1,
            'wikiDataId' => 'Q203193',
        ];
    }
}
