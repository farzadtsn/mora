<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_city', function (Blueprint $table) {
            $table->id();
            $table->foreignId('city_id')->index();
            $table->foreignId('business_id')->index();
            $table->timestamps();

            $table->unique(['city_id', 'business_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_city');
    }
};
