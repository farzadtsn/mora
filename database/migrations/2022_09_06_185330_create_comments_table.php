<?php

use App\Enums\ApproveStateEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->id();
            $table->morphs('commentable');
            $table->text('body', 1500);
            $table->string('state_enum', 30)->default(ApproveStateEnum::PENDING);
            $table->string('reject_reason', 250)->nullable();
            $table->float('rating', 2,1)->comment('point from 5');
            $table->foreignId('user_id')->index();
            $table->unsignedBigInteger('parent_id')->nullable()->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
};
