<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Currency;
use App\Models\PaymentGateway;
use App\Models\User;
use DB;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(CategoryTableSeeder::class);

        $this->call(CurrencyTableSeeder::class);
        $this->call(PaymentGatewayTableSeeder::class);
        $this->call(UserBalanceTableSeeder::class);
        $this->call(UserTransactionTableSeeder::class);
        $this->call(FacilityTableSeeder::class);

        Artisan::call('mono:countries');
        $this->call(CoreUserSeeder::class);
        $this->call(DiscountCampaignTableSeeder::class);
        $currencyIds = Currency::pluck('id')->toArray();

        PaymentGateway::all()->each(function ($gateway) use ($currencyIds) {
            $gateway->currencies()->attach($currencyIds);
        });
    }
}
