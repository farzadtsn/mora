<?php
declare(strict_types=1);

use App\Models\DiscountCampaign;
use Elastic\Adapter\Indices\Mapping;
use Elastic\Adapter\Indices\Settings;
use Elastic\Migrations\Facades\Index;
use Elastic\Migrations\MigrationInterface;

final class CreateDiscountCampaignIndex implements MigrationInterface
{
    /**
     * Run the migration.
     */
    public function up(): void
    {
        $mapping = [
            'properties' => [
                'name' => [
                    'type' => 'text',
                    'fields' => [
                        'raw' => [
                            'type' => 'keyword',
                        ]
                    ]
                ],
                'variants' => [
                    'type' => 'nested',
                ],
                'categories' => [
                    'type' => 'nested',
                ],
                'business.name' => [
                    'type' => 'text',
                    'fields' => [
                        'raw' => [
                            'type' => 'keyword',
                        ]
                    ]
                ],
                'business.location' => [
                    'type' => 'geo_point',
                    'fields' => [
                        'raw' => [
                            'type' => 'keyword',
                        ]
                    ]
                ],
            ]
        ];

        Index::createIfNotExistsRaw((new DiscountCampaign())->searchableAs(), $mapping);
    }

    /**
     * Reverse the migration.
     */
    public function down(): void
    {
        Index::dropIfExists((new DiscountCampaign())->searchableAs());
    }
}
