<?php

namespace Tests\Feature\Controllers\BaseInfo;

use App\Models\City;
use App\Models\Country;
use App\Models\State;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class CountryControllerTest extends TestCase
{
    use DatabaseTransactions;

    public function test_can_view_country()
    {
        $country = Country::factory()->make();

        $response = $this->get("api/base-info/countries/{$country->id}");

        $response->assertStatus(200);
    }

    public function test_can_view_countries()
    {
        Country::factory()->make();

        $response = $this->get('api/base-info/countries');

        $response->assertStatus(200);
    }

    public function test_can_view_states_of_country()
    {
        $state = State::factory()->make();

        $response = $this->get("api/base-info/countries/{$state->country_id}/states");

        $response->assertStatus(200);
    }

    public function test_can_view_cities_of_country()
    {
        $city = City::factory()->make();

        $response = $this->get("api/base-info/countries/{$city->country_id}/cities");

        $response->assertStatus(200);
    }
}
