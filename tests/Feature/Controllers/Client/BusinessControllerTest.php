<?php

namespace Tests\Feature\Controllers\Client;

// use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Models\Category;
use App\Models\City;
use App\Models\Country;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class BusinessControllerTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp(): void
    {
        parent::setUp();

        $this->category = Category::create([
            'name' => 'Test Category',
        ]);
        $user = User::first();
        $user->business->update(['step' => 3]);
        $this->business = $user->business;
    }

    public function test_business_can_set_info()
    {
        $inputs = [
            'business_name' => 'monopon',
            'website' => 'http://monopon.com',
            'categories' => [$this->category->id],
            'business_type' => 'company',
        ];
        $response = $this->post('api/client/business/info', $inputs);

        $response->assertStatus(200);

        $business = $this->business->refresh();

        $this->assertEquals($inputs, [
            'business_name' => $business->name,
            'website' => $business->website,
            'categories' => $business->categories->pluck('id')->toArray(),
            'business_type' => $business->business_type,
        ]);
    }

    public function test_business_can_set_address()
    {
        $city = City::factory()->create();
        $inputs = [
            'city_id' => $city->id,
            'zip_code' => '123123213',
            'address' => 'test street',
        ];
        $response = $this->post('api/client/business/address', $inputs);

        $response->assertStatus(200);

        $business = $this->business->refresh();

        $this->assertEquals(array_merge($inputs, [
            'state_id' => $city->state_id,
            'country_id' => $city->country_id,
        ]), [
            'city_id' => $business->city_id,
            'zip_code' => $business->zip_code,
            'address' => $business->address,
            'state_id' => $business->state_id,
            'country_id' => $business->country_id,
        ]);
    }

    public function test_business_can_set_phone()
    {
        $country = Country::factory()->create(['iso2' => 'IR']);

        $this->business->update([
            'country_id' => $country->id,
        ]);

        $inputs = [
            'phone' => '989123456789',
        ];

        $response = $this->post('api/client/business/phone', $inputs);

        $response->assertStatus(200);

        $business = $this->business->refresh();

        $this->assertEquals($inputs, [
            'phone' => $business->phone,
        ]);
    }
}
