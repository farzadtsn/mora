<?php

namespace Tests;

use App\Models\User;
use Database\Seeders\CoreUserSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\Http;
use Symfony\Component\HttpFoundation\Response;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        $this->seed(CoreUserSeeder::class);

        $user = User::first();

        $user->business()->create();

        $fakeData = json_decode(file_get_contents(base_path('tests/data/verify.json')), true);
        $fakeData['data']['user']['id'] = $user->id;
        Http::fake([
            config('auth-service.service-url') . '/api/auth/verify' => Http::response(
                $fakeData,
                Response::HTTP_OK,
            ),
        ]);
    }
}
